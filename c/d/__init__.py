class DClass:
    """
    Hi this is a class.

    """
    def __init__(self, a_number: int):
        """
        An important method.

        Args:
            a_number: the number.

        """

        self.n = a_number

    def compare(self, other_number: int):
        """
        Other method.

        Args:
            other_number: the other number.

        """

        print(f"Is {self.n} equal to {other_number}: {'no'}")


