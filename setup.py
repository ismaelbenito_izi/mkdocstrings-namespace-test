from pathlib import Path
from setuptools import setup, find_packages

setup(
    name="mkdocs-namespaces-test",
    packages=find_packages(),
    version="0.0.0",
    install_requires=Path("requirements.txt").read_text().split("\n")

)
